'use client';

import Image from 'next/image'
import Counter from './components/counter'
import { use } from 'react'
import { invoke } from '@/lib/tauri'

export default function Home() {
  // now we can call our Command!
  // Right-click the application background and open the developer tools.
  // You will see "Hello, World!" printed in the console!
  invoke('greet', { name: 'World' })
  // `invoke` returns a Promise
  .then((response) => console.log(response))

  return (
    <div>
      <Counter />
    </div>
  )
}
