import React, { useState } from "react";
import { Button } from "@/app/components/ui/button";
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/app/components/ui/card";
import { Separator } from "@/app/components/ui/separator";
import { Label } from "@/app/components/ui/label";

const Counter = () => {
    const [count, setCount] = useState(0)
    return (
        <div className="flex h-screen">
            <div className="m-auto">
                <Card className="w-[600px]">
                    <CardHeader>
                        <CardTitle>Simple Component</CardTitle>
                        <CardDescription>Add and Subtract Values</CardDescription>
                    </CardHeader>
                    <Separator />
                    <CardContent className="my-4">
                        <div className="flex justify-center">
                            <Label htmlFor="count" className="text-5xl">{count}</Label>
                        </div>
                    </CardContent>
                   
                    <CardFooter className="flex gap-4 justify-end">
                        <Button variant="ghost" type="button" onClick={() => setCount((count) => count - 1)}>Subtract</Button>
                        <Button variant="ghost" type="button" onClick={() => setCount((count) => count + 1)}>
                            Add
                        </Button>
                    </CardFooter>
                </Card>

            </div>
        </div>
    )
}
export default Counter;